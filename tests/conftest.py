"""Test fixtures."""

import tempfile

import pytest

from pycapitalist.ledger import Ledger


@pytest.fixture
def ledger():
    """A mock ledger file."""
    contents = (
        "1971-01-01,ayn,milton,666.00\n"
        "1971-01-15,milton,pinochet,20.42\n"
        "1971-01-17,pinochet,walmart,15.10\n"
    )
    with tempfile.NamedTemporaryFile() as tmpfile:
        tmpfile.write(bytes(contents, 'utf-8'))
        tmpfile.flush()
        yield Ledger(tmpfile.name)
