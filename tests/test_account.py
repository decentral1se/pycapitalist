"""Account unit tests."""

import datetime
from decimal import Decimal

import pytest

from pycapitalist.account import Account
from pycapitalist.exception import InvalidFieldType


def test_account_validation(ledger):
    with pytest.raises(InvalidFieldType):
        Account(ledger=None, identifier='foobar')

    with pytest.raises(InvalidFieldType):
        Account(ledger=ledger, identifier=32)


def test_account(ledger):
    account = Account(ledger=ledger, identifier='Ayn Rand')
    assert account.ledger == ledger
    assert account.identifier == 'Ayn Rand'
    assert account.transactions == []


def test_account_getter(ledger):
    ledger.process()
    assert ledger.get_account('milton') is not None
    assert ledger.get_account('NOT-THERE') is None


def test_account_query(ledger):
    ledger.process()
    account = ledger.get_account('milton')

    result = account.query(amount=Decimal('666.00'))
    assert len(result) == 1

    result = account.query(creditor='DOESNT-EXIST')
    assert len(result) == 0

    def fifteenth(date):
        return date < datetime.date(year=1971, month=1, day=15)
    result = account.query(debtor='ayn', date=fifteenth)
    assert len(result) == 1

    def first(date):
        return date < datetime.date(year=1966, month=1, day=1)
    result = account.query(amount=Decimal('42.00'), date=first)
    assert len(result) == 0


def test_account_balance(ledger):
    ledger.process()

    ayns_account = ledger.get_account('ayn')
    assert ayns_account.balance() == Decimal('-666.00')

    miltons_account = ledger.get_account('milton')
    assert miltons_account.balance() == Decimal('645.58')

    first = datetime.date(year=1971, month=1, day=1)
    assert miltons_account.balance(date=first) == Decimal('666.00')

    fifteenth = datetime.date(year=1971, month=1, day=15)
    assert miltons_account.balance(date=fifteenth) == Decimal('645.58')

    pinochets_accout = ledger.get_account('pinochet')
    assert pinochets_accout.balance() == Decimal('5.32')

    walmarts_account = ledger.get_account('walmart')
    assert walmarts_account.balance() == Decimal('15.10')
