"""Transaction unit tests."""

import datetime
from datetime import datetime as dt
from decimal import Decimal

import pytest

from pycapitalist.account import Account
from pycapitalist.exception import InvalidFieldType
from pycapitalist.transaction import Transaction


def test_transaction_validation(ledger):
    acc = Account(ledger=ledger, identifier='ayn')
    invalid_args = (
        ('foobar', dt.today(), acc.identifier, 'Milton', Decimal('42.67')),
        (acc, 'foobar', acc.identifier, 'Milton', Decimal('42.67')),
        (acc, dt.today, '', 'Milton', Decimal('42.67')),
        (acc, dt.today, acc.identifier, 666, Decimal('42.67')),
        (acc, dt.today, acc.identifier, 'Milton', 'foobar'),
    )
    for args in invalid_args:
        with pytest.raises(InvalidFieldType):
            Transaction(*args)


def test_transaction(ledger):
    account = Account(ledger=ledger, identifier='Ayn Rand')
    transaction = Transaction(
        account=account,
        date=dt.today(),
        debtor=account.identifier,
        creditor='Milton Friedman',
        amount=Decimal('42.67')
    )
    assert isinstance(transaction.date, datetime.date)
    assert isinstance(transaction.account, Account)


def test_transaction_amount_sign(ledger):
    ledger.process()
    account = ledger.get_account('ayn')

    result = Transaction.determine_amount_sign(account, 'ayn', Decimal('1.0'))
    assert result == Decimal('-1.0')

    result = Transaction.determine_amount_sign(account, 'foo', Decimal('1.0'))
    assert result == Decimal('1.0')
