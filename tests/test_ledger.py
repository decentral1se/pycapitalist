"""Ledger unit tests."""

import tempfile

import pytest

from pycapitalist.exception import MissingLedgerFile, UnreadableLedgerFile
from pycapitalist.ledger import Ledger


def test_ledger_validation():
    with pytest.raises(MissingLedgerFile):
        Ledger("DOESNT-EXIST")

    with tempfile.TemporaryDirectory() as tmp_path:
        with pytest.raises(UnreadableLedgerFile):
            Ledger(tmp_path)

    with pytest.raises(FileNotFoundError):
        with tempfile.NamedTemporaryFile() as tmpfile:
            ledger = Ledger(tmpfile.name)
        ledger.process()


@pytest.mark.parametrize('entry', (
    "BAD-DATE,pinochet,walmart,15.10\n",
    "1971-01-17,pinochet,walmart,BAD-DECIMAL\n",
    "\n", ""
))
def test_ledger_badly_formed_entry(entry):
    with tempfile.NamedTemporaryFile() as tmpfile:
        tmpfile.write(bytes(entry, 'utf-8'))
        tmpfile.flush()

        ledger = Ledger(tmpfile.name)
        ledger.process()

    skipped_because_empty = len(entry.strip()) == 0
    if not skipped_because_empty:
        assert len(ledger.accounts) == 2
        for identifier in ('pinochet', 'walmart'):
            account = ledger.get_account(identifier)
            assert account.transactions == []


def test_ledger_process_skipping_logs(caplog):
    contents = "1971-01-17,foo,bar,0.0,TOO-MUCH\n"
    with tempfile.NamedTemporaryFile() as tmpfile:
        tmpfile.write(bytes(contents, 'utf-8'))
        tmpfile.flush()

        ledger = Ledger(tmpfile.name)
        ledger.process()

    expected_msg = 'due to bad formatting.'
    assert any((expected_msg in rec.msg for rec in caplog.records()))


def test_ledger_process(ledger):
    ledger.process()
    assert len(ledger.accounts) == 4

    ayns_account = ledger.get_account('ayn')
    assert len(ayns_account.transactions) == 1

    miltons_account = ledger.get_account('milton')
    assert len(miltons_account.transactions) == 2

    pinochets_account = ledger.get_account('pinochet')
    assert len(pinochets_account.transactions) == 2

    walmarts_account = ledger.get_account('walmart')
    assert len(walmarts_account.transactions) == 1


def test_ledger_mtime_skips(ledger, caplog):
    assert ledger.mtime is None
    ledger.process()
    assert ledger.mtime is not None

    ledger.process()
    expected_msg = 'No new ledger entries'
    assert any((expected_msg in rec.msg for rec in caplog.records()))

    with open(ledger.path, 'w') as fhandle:
        fhandle.write('foobar')
        fhandle.flush()

    ledger.process()
    expected_msg = 'Commencing ledger processing'
    assert any((expected_msg in rec.msg for rec in caplog.records()))
