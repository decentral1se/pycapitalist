"""Package configuration."""

from setuptools import find_packages, setup

setup(
    name='pycapitalist',
    version='6.6.6',
    packages=find_packages('.', exclude=["tests"]),
)
