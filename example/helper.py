"""Helper functions for hacking in the REPL."""

import datetime  # noqa
import os
from decimal import Decimal  # noqa
from pprint import pprint  # noqa

from pycapitalist.ledger import Ledger


def getdate(iso_date):
    """Build quick datetime.date from ISO strings."""
    ks, vs = (('year', 'month', 'day'), iso_date.split("-"))
    kwargs = {k: int(v) for k, v in zip(ks, vs)}
    return datetime.date(**kwargs)


def load_ledger():
    """Load up the ledger for hacking."""
    fpath = os.path.join(os.getcwd(), 'example.ledger')
    abspath = os.path.abspath(fpath)
    return Ledger(abspath)


ledger = load_ledger()
ledger.process()
