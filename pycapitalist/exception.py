"""Custom exception classes."""


class MissingLedgerFile(Exception):
    """The ledger file cannot be found."""


class UnreadableLedgerFile(Exception):
    """The supplied ledger file cannot be read."""


class InvalidFieldType(Exception):
    """A value of invalid type was passed."""
