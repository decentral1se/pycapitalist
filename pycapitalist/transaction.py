"""The Transaction module."""

import datetime
from decimal import Decimal

from pycapitalist.account import Account
from pycapitalist.exception import InvalidFieldType


class Transaction():
    """The Transaction."""

    # The number of values that should be specified
    # in a transaction definition in the ledger file
    # e.g. <date>,<debtor>,<creditor>,<amount>
    TRANSACATION_FORMAT_LENGTH = 4

    # Indices for accessing field values during ledger processing
    DATE_INDEX = 0
    DEBTOR_INDEX = 1
    CREDITOR_INDEX = 2
    AMOUNT_INDEX = 3

    def __init__(self, account, date, debtor, creditor, amount):
        """Object initialisation."""
        self.validate(account, date, debtor, creditor, amount)

        self.account = account
        self.date = date
        self.debtor = debtor
        self.creditor = creditor
        self.amount = self.determine_amount_sign(account, debtor, amount)

    def validate(self, account, date, debtor, creditor, amount):
        """Guarantee object initialisation can proceed."""
        msg = "Argument {} is not of type {}"
        if not isinstance(account, Account):
            raise InvalidFieldType(msg.format("'account'", "'Account'"))
        if not isinstance(date, datetime.date):
            raise InvalidFieldType(msg.format("'date'", "'datetime.datetime'"))
        if not isinstance(debtor, str):
            raise InvalidFieldType(msg.format("'debtor'", "'str'"))
        if not isinstance(creditor, str):
            raise InvalidFieldType(msg.format("'creditor'", "'str'"))
        if not isinstance(amount, Decimal):
            raise InvalidFieldType(msg.format("'amount'", "'Decimal'"))

    @classmethod
    def determine_amount_sign(cls, account, debtor, amount):
        """Determine which sign (+, -) is used for transaction amount.

        Since each Transaction is stored within an Account, the amount should
        vary depending on whether the Account holder in question is the debtor
        or creditor in the Transaction.
        """
        amount = str(amount)

        if any(sign in amount for sign in ('-', '+')):
            strip_sign = slice(1, len(amount))
            amount = amount[strip_sign]

        if debtor == account.identifier:
            return Decimal('-' + str(amount))

        return Decimal(amount)

    def __repr__(self):
        """Object representation."""
        obj_repr = (
            "<Transaction: account={}, date={}, "
            "debtor={}, creditor={}, amount={}>"
        )
        return obj_repr.format(
            self.account.identifier,
            self.date,
            self.debtor,
            self.creditor,
            self.amount
        )
