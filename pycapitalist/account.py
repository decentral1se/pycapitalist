"""The Account module."""

import operator
from decimal import Decimal
from functools import reduce
from types import FunctionType

from pycapitalist.exception import InvalidFieldType
from pycapitalist.ledger import Ledger


class Account():
    """The account."""

    def __init__(self, ledger, identifier):
        """Object initialisation."""
        self.validate(ledger, identifier)

        self.ledger = ledger
        self.identifier = identifier
        self.transactions = []

    def validate(self, ledger, identifier):
        """Guarantee object initialisation can proceed."""
        if not isinstance(ledger, Ledger):
            msg = "Argument 'ledger' is not of type 'Ledger'"
            raise InvalidFieldType(msg)
        if not isinstance(identifier, str):
            msg = "Argument 'identifier' is not of type 'str'"
            raise InvalidFieldType(msg)

    def query(self, **kwargs):
        """Query for transactions in this account.

        `kwargs` can be a mixture of attribute=value keyword arguments or a
        predicate function which accepts a single argument (the attribute
        value) and returns a boolean based on the function body. Evaluation
        behaves like a logical AND (for all `kwargs` passed).
        """
        matching = []

        for transaction in self.transactions:
            for attr, value in kwargs.items():
                predicates = []
                accessed_value = getattr(transaction, attr)

                if isinstance(value, FunctionType):
                    predicates.append(value(accessed_value))
                else:
                    predicates.append(accessed_value == value)

            if all(predicates):
                matching.append(transaction)

        return matching

    def balance(self, date=None):
        """Get the account balance.

        If a `date` is passed, return the balance up to that date inclusive.
        Otherwise, it returns the total account balance.
        """
        transactions = (
            self.transactions if date is None else
            self.query(date=lambda d: d <= date)
        )
        amounts = [t.amount for t in transactions]
        return reduce(operator.add, amounts, Decimal('0'))

    def __repr__(self):
        """Object representation."""
        return "<Account: id={}>".format(self.identifier)
