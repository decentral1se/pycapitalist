"""The Ledger module."""

import logging
import os
from datetime import datetime as dt
from decimal import Decimal, InvalidOperation

from pycapitalist.exception import (InvalidFieldType, MissingLedgerFile,
                                    UnreadableLedgerFile)

logger = logging.getLogger(__name__)


class Ledger():
    """The Ledger."""

    def __init__(self, path):
        """Object initialisation."""
        self.validate(path)

        self.path = path
        self.accounts = []
        self.mtime = None

    def validate(self, path):
        """Guarantee object initialisation can proceed."""
        if not os.path.exists(path):
            raise MissingLedgerFile()
        if not os.path.isfile(path):
            raise UnreadableLedgerFile()

    def get_account(self, identifier):
        """Get an account by identifier."""
        try:
            return next(filter(
                lambda account: account.identifier == identifier,
                self.accounts
            ))
        except StopIteration:
            return None

    def create_accounts(self, values):
        """Create accounts during ledger processing.

        Accounts are created for both debtor and creditor by passing
        iteratively through the transaction values. Existing accounts are
        skipped.
        """
        from pycapitalist.transaction import Transaction
        from pycapitalist.account import Account

        debtor = values[Transaction.DEBTOR_INDEX]
        creditor = values[Transaction.CREDITOR_INDEX]

        for identifier in (debtor, creditor):
            if self.get_account(identifier) is None:
                new_account = Account(ledger=self, identifier=identifier)
                self.accounts.append(new_account)

    def create_transactions(self, values):
        """Create transactions during ledger processing.

        Entries with bad values are simply skipped.
        """
        from pycapitalist.transaction import Transaction

        try:
            debtor = values[Transaction.DEBTOR_INDEX]
            creditor = values[Transaction.CREDITOR_INDEX]
            amount = Decimal(values[Transaction.AMOUNT_INDEX])
            date = dt.strptime(
                values[Transaction.DATE_INDEX], "%Y-%M-%d"
            ).date()

            for identifier in (debtor, creditor):
                account = self.get_account(identifier)
                args = (account, date, debtor, creditor, amount)
                transaction = Transaction(*args)
                account.transactions.append(transaction)
        except (ValueError, InvalidOperation,
                IndexError, InvalidFieldType) as exception:
            msg = 'Skipping transaction creation, saw: {}'
            logger.info(msg.format(str(exception)))

    @property
    def get_mtime(self):
        return os.stat(self.path).st_mtime

    def set_mtime(self):
        self.mtime = self.get_mtime

    def process(self):
        """Process the ledger file.

        All entries in the ledger file will correspond to accounts and
        transactions after successful processing. Badly formatted lines are
        skipped. The modified time of the ledger file is stored to avoid
        re-processing.
        """
        from pycapitalist.transaction import Transaction

        if self.mtime is not None and self.get_mtime == self.mtime:
            logger.info('No new ledger entries since last process. Skipping.')
            return
        logger.info('Commencing ledger processing now.')

        try:
            handle = open(self.path, encoding='utf-8', errors='strict')
        except (FileNotFoundError, UnicodeDecodeError) as exception:
            raise exception

        for count, line in enumerate(handle.readlines()):
            separated = line.split(',')
            values = list(map(lambda value: value.strip(), separated))

            if not len(values) == Transaction.TRANSACATION_FORMAT_LENGTH:
                msg = 'Skipping transaction (line {}) due to bad formatting.'
                logger.info(msg.format(count))
                continue

            self.create_accounts(values)
            self.create_transactions(values)

        self.set_mtime()

    def __repr__(self):
        """Object representation."""
        return "<Ledger: {}>".format(self.path)
