# pycapitalist

I was asked to write some Python code for a job interview once and I really
must admit that I am quite proud of it. So, for anyone who cares, here is a
small program that I wrote in a handful of hours that parses a ledger file and
builds up a small in-memory model of the accounts, transactions and provides a
transaction query interface.

You can see me playing around with it over at https://asciinema.org/a/132997.

If you want to do something with it, use [pipenv] to get moving:

``` bash
$ pipenv install --three --dev # install everything you need
$ make sureitworks             # make sure your change works
$ make replhack                # play around in the bPython REPL
```

[pipenv]: http://docs.pipenv.org/en/latest/advanced.html#fancy-installation-of-pipenv
