PIPENVRUN := pipenv run
SOURCEDIR := pycapitalist/
TESTDIR   := tests/

test:
	@$(PIPENVRUN) pytest --disable-pytest-warnings -q $(TESTDIR)
.PHONY: test

quality:
	@$(PIPENVRUN) pylama $(SOURCE_DIR)
	@find $(SOURCE_DIR) -name "*.py" | xargs $(PIPENVRUN) isort -c --diff
.PHONY: quality

sureitworks: quality test
.PHONY: sureitworks

replhack:
	@cd example/ && pipenv run bpython -i helper.py
.PHONY: replhack
